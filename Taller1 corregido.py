#Ejercicio 1
import math #Importa math, para usar después la función de potencia
def Hazard(n):
    i=1
    j=0
    while i<=n:
       a = (math.pow(-1, i+1))*2
       b = (2*i)-1
       d = a/b #Devuelve el término del sumando 
       j=j+d #Suma a lo obtenido hasta el momento, otro término
       i+=1
    return j
print(Hazard(7))

#Ejercicio 2
def esPrimo(x):
    i=1
    j=0
    while i<=x:
        if x%i==0:
            j=j+1 #Si x es divisible por i, suma un número
        i+=1 #Verifica con todos los números, del 1 al mismo, si es divisibles por ellos
    if j==2: #Es decir, si es primo. Si no fuera primo, j debería ser igual a 3 o más si no si no tenemos en cuenta al 1
        return True 
    else:
        return False

def Lukaku(n):
    res=0
    i=1
    j=0
    while j<n:
        aux=2**i-1
        if esPrimo(aux)==True:
            j+=1
            res=aux
        i+=1
    return res
print(Lukaku(3))