#Ejercicio 1
import math #Importa math, para usar después la función de potencia
def Hazard(n):
    i=1
    j=0
    while i<len(n):
       a = (math.pow(-1, i+1))*2
       b = (2*i)-1
       d = b/c #Devuelve el término del sumando 
       j=j+d #Suma a lo obtenido hasta el momento, otro término
       i+=1
    res=j
    return res
lista = [1, 2, 3, 4]
Hadard(lista)


#Ejercicio 2
#Ya estaba importado math del ejercicio anterior, si no estuviera, hay que importarlo
def es2N1(x):
    n=math.log(x+1, 2) #Reordenando (2^n)-1=x da que n es igual a log en base 2 de x+1
    if n>=0: #Solo si n>=0 es True, se puede seguir con el enunciado segun la especificación
        return True

def esPrimo(x):
    i=1
    j=0
    while i<=x:
        if x%i==0:
            j=j+1 #Si x es divisible por i, suma un número
        i+=1 #Verifica con todos los números, del 1 al mismo, si es divisibles por ellos
    if j==2: #Es decir, si es primo. Si no fuera primo, j debería ser igual a 3 o más si no si no tenemos en cuenta al 1
        return True 
    else:
        return False

def Lukaku(n):
    res=0
    i=1
    j=0
    while res<130: #Se le puso un límite de 130 para que no sea infinito, y funcione con el tutor, aunque la función termina cuando de un 'res', pero se le puede poner un valor más grande
        if es2N1(res)==True: #Si el número del ciclo da True, continua
            if esPrimo(res)==True: #Si el número del ciclo da True, continua
                while i<=res:
                    if es2N1(i)==True:
                        if esPrimo(i)==True:
                            j+=1
                    i+=1
                    if j==n:
                        return res
        res+=1